# 微信报名抽奖并发送中奖消息模板

#### 介绍
微信 关注报名 提交姓名手机 ，记录用户OPENID  昵称 等相关字段，大屏幕从数据库中随机取 中奖人员并发送模板消息

#### 软件架构
微信报名 抽奖  发送模板消息 


#### 安装教程

1. $APPID  ,$APPSECRET 此文件多处用到 请替换自己的企业号对应的参数
2. 数据库 两个表 一个是记录access_token 这个有时间限制7200秒所以要记录防止过期失效 表名yn_wx_acctoken两个字段一个是时间一个是access_token 
3. 报名记录表 magc_wx_jinl 相关字段(openid,name,tel,nickname,province,city,headimgurl,subscribe_time）为了后期抽奖从中抽取


#### 使用说明

1. 有问题加我 QQ1655099 互相学习指教
2. xxxx
3. xxxx

#### 参与贡献

1. Fork 本仓库
2. 新建 Feat_xxx 分支
3. 提交代码
4. 新建 Pull Request


#### 码云特技

1. 使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2. 码云官方博客 [blog.gitee.com](https://blog.gitee.com)
3. 你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解码云上的优秀开源项目
4. [GVP](https://gitee.com/gvp) 全称是码云最有价值开源项目，是码云综合评定出的优秀开源项目
5. 码云官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6. 码云封面人物是一档用来展示码云会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)